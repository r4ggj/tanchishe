function Cell(r,c){
	this.r=r;
	this.c=c;
}
var tanchishe={
	RN:25,//��������
	CN:50,//��������
	direct:null,//�����ƶ�����
	wall:null,//������Ļ�����и���
	SIZE:20,//����һ������ĳߴ�
	TOLEFT:-2,
	TOTOP:-1,
	TORIGHT:2,
	TOBOTTOM:1,
	state:null,
	RUNNING:1,
	PAUSED:2,
	GAMEOVER:0,
	timer:null,//��ʱ��
	bg:null,//����ҳ�淶Χdiv
	cells:null,//���浱ǰ����
	nextShape:null,//������ͷ��һ��λ��
	randShape:null,//����������ɵķ���
	init:function(){//��ʼ��
		this.direct=this.TOLEFT;//��ʼ���ƶ�����
		this.cells=[new Cell(12,24),new Cell(12,25),new Cell(12,26)];//��ʼ����ʼ������
		this.bg=document.getElementById("bg");

	},
	random:function(){//��Ļ���������һ������
		
		for(var bool=true;;){//����������飬ֱ�����������ص�;
			var r=parseInt(Math.random()*this.RN);
			var c=parseInt(Math.random()*this.CN);
			this.randShape=new Cell(r,c);
			for(var i=0;i<this.cells.length;i++){
				if(this.randShape.r==this.cells[i].r&&this.randShape.c==this.cells[i].c){
					bool=false;
					break;	
				}
			}
			if(bool==true){
				this.wall=this.randShape;
				return true;
			}
			
		}
	},
	start:function(){//��Ϸ��ʼ
		this.state=this.RUNNING;
		this.init();
		this.random();
		this.paintStart();
		this.timer=setInterval(this.move.bind(this),250);
		window.onkeydown=this.keyDown.bind(this);
	},
	keyDown:function(e){//��Ӧ�����¼�
		switch (e.keyCode)
		{
		case 37:
			if(this.state==this.RUNNING&&this.direct!==this.TORIGHT){this.direct=this.TOLEFT;this.move();}break;
		case 38:
			if(this.state==this.RUNNING&&this.direct!==this.TOBOTTOM){this.direct=this.TOTOP;this.move();}break;
		case 39:
			if(this.state==this.RUNNING&&this.direct!==this.TOLEFT){this.direct=this.TORIGHT;this.move();}break;
		case 40:
			if(this.state==this.RUNNING&&this.direct!==this.TOTOP){this.direct=this.TOBOTTOM;this.move();}break;
		case 80:
			if(this.state==this.RUNNING){clearInterval(this.timer),this.timer=null;this.state=this.PAUSED;}break;
		case 67:
			if(this.state==this.PAUSED){this.timer=setInterval(this.move.bind(this),250);this.state=this.RUNNING;}break;
		case 83:
			if(this.state==this.GAMEOVER){this.start();}break;
		}
	},
	canMove:function(){
		for(var i=0;i<this.cells.length;i++){//��ͷ����ײ���Լ�
			if((this.nextShape.r==this.cells[i].r&&this.nextShape.c==this.cells[i].c)||!(0<=this.nextShape.r&&this.nextShape.r<this.RN)||!(0<=this.nextShape.c&&this.nextShape.c<this.CN)){
				clearInterval(this.timer);
				this.timer=null;
				this.state=this.GAMEOVER;
				return;
				
			}
		}
		return true;
	},
	move:function(){//�����ƶ��ķ���
			this.direct==this.TOLEFT&&this.moveLeft();
			this.direct==this.TOTOP&&this.moveTop();
			this.direct==this.TORIGHT&&this.moveRight();
			this.direct==this.TOBOTTOM&&this.moveBottom();
	},
	moveLeft:function(){//����
		var cell=this.cells[0];//���浱ǰ��ͷ
		this.nextShape=new Cell(cell.r,cell.c-1);//������һ����ͷ��λ��
		if(this.canMove()){//�ж��ܹ��ƶ���������һ��
			if(this.wall.r==this.nextShape.r&&this.wall.c==this.nextShape.c){
				//�ж���һ����ͷ���ֵ�λ����wall���Ƿ�Ϊ������ɵķ��飬����ǣ�
				this.cells.unshift(this.nextShape);//����һ����ͷѹ������
				this.nextShape=new Cell
				var cell=this.cells[0];//���浱ǰ��ͷ
				this.nextShape=new Cell(cell.r,cell.c-1);//������һ����ͷ��λ��
				this.wall=null;//���wall�е��������
				this.random();//���������������
				//����ҳ��
				this.paintStart();
			}
				this.cells.pop();//ȥ����β
				this.cells.unshift(this.nextShape);//ѹ����ͷ
				this.paintStart();//����ҳ��
		}
	},
	moveTop:function(){//����
		var cell=this.cells[0];
		this.nextShape=new Cell(cell.r-1,cell.c);
		if(this.canMove()){//�ж��ܹ��ƶ���������һ��
			if(this.wall.r==this.nextShape.r&&this.wall.c==this.nextShape.c){
				//�ж���һ����ͷ���ֵ�λ����wall���Ƿ�Ϊ������ɵķ��飬����ǣ�
				this.cells.unshift(this.nextShape);//����һ����ͷѹ������
				var cell=this.cells[0];
				this.nextShape=new Cell(cell.r-1,cell.c);
				this.wall=null;//���wall�е��������
				this.random();//���������������
				this.paintStart();//����ҳ��

			}
				this.cells.pop();//ȥ����β
				this.cells.unshift(this.nextShape);//ѹ����ͷ
				this.paintStart();//����ҳ��
		}
		
	},
	moveRight:function(){//����
		var cell=this.cells[0];
		this.nextShape=new Cell(cell.r,cell.c+1);
		if(this.canMove()){//�ж��ܹ��ƶ���������һ��
			if(this.wall.r==this.nextShape.r&&this.wall.c==this.nextShape.c){
				//�ж���һ����ͷ���ֵ�λ����wall���Ƿ�Ϊ������ɵķ��飬����ǣ�
				this.cells.unshift(this.nextShape);//����һ����ͷѹ������
				var cell=this.cells[0];
				this.nextShape=new Cell(cell.r,cell.c+1);
				this.wall=null;//���wall�е��������
				this.random();//���������������
				this.paintStart();//����ҳ��

			}
				this.cells.pop();//ȥ����β
				this.cells.unshift(this.nextShape);//ѹ����ͷ
				this.paintStart();//����ҳ��
		}
		
	},
	moveBottom:function(){//����
		var cell=this.cells[0];
		this.nextShape=new Cell(cell.r+1,cell.c);
		if(this.canMove()){//�ж��ܹ��ƶ���������һ��
			if(this.wall.r==this.nextShape.r&&this.wall.c==this.nextShape.c){
				//�ж���һ����ͷ���ֵ�λ����wall���Ƿ�Ϊ������ɵķ��飬����ǣ�
				this.cells.unshift(this.nextShape);//����һ����ͷѹ������
				var cell=this.cells[0];
				this.nextShape=new Cell(cell.r+1,cell.c);
				this.wall=null;//���wall�е��������
				this.random();//���������������
				this.paintStart();//����ҳ��

			}
				this.cells.pop();//ȥ����β
				this.cells.unshift(this.nextShape);//ѹ����ͷ
				this.paintStart();//����ҳ��
		}
		
	},
	paintStart:function(){//��������ͼ��
		this.bg.innerHTML="";
		var frag=document.createDocumentFragment();
		
		for(var i=0;i<this.cells.length;i++){
			var cell=this.cells[i]
			var div=document.createElement("div");
			div.style.left=cell.c*this.SIZE+"px";
			div.style.top=cell.r*this.SIZE+"px";
			frag.appendChild(div);
		}
		if(this.randShape!==undefined){
			var div1=document.createElement("div");
			div1.style.left=this.randShape.c*this.SIZE+"px";
			div1.style.top=this.randShape.r*this.SIZE+"px";
			frag.appendChild(div1);
		}
		this.bg.appendChild(frag);
	},
};
window.onload=function(){tanchishe.start();}